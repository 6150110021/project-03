package com.example.projectcustomer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterInterface {

    public static String BASE_URL = "https://itlearningcenters.com/android/project0309/";
    @FormUrlEncoded
    @POST("regist_cust.php")
    public Call<ResponseBody> registerUser(@Field("cus_id") String cusIdValue,
                                           @Field("username") String userNameValue,
                                           @Field("password") String passwordValue,
                                           @Field("cus_name") String cusNameValue,
                                           @Field("cus_phone") String cusPhoneValue,
                                           @Field("email") String emailValue,
                                           @Field("cus_address") String cusAddressValue);

        public static String LogBASE_URL = "https://itlearningcenters.com/android/project0309/";
    @FormUrlEncoded
    @POST("cust_login.php")
    Call<ResponseBody> login( @Field("username") String userNameValue,
                              @Field("password") String passwordValue);

}
