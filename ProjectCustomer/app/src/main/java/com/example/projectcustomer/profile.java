package com.example.projectcustomer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


    }
    public void logout(View view) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void change(View view) {
        Intent intent = new Intent(this,profile.class);
        startActivity(intent);
    }

    public void edit(View view) {
        Intent intent = new Intent(this,profile.class);
        startActivity(intent);
    }

    public void menu(View view) {
        Intent intent = new Intent(this, menu.class);
        startActivity(intent);
    }
}