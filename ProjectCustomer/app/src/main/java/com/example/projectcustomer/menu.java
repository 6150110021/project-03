package com.example.projectcustomer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class menu extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void history(View view) {
        Intent intent = new Intent(this, history.class);
        startActivity(intent);
    }

    public void seeRaward(View view) {
        Intent intent = new Intent(this, reward.class);
        startActivity(intent);
    }

    public void seeStamp(View view) {
        Intent intent = new Intent(this, stamp.class);
        startActivity(intent);
    }

    public void profile(View view) {
        Intent intent = new Intent(this, profile.class);
        startActivity(intent);
    }

    public void seeReward(View view) {
        Intent intent = new Intent(this, reward.class);
        startActivity(intent);
    }

    public void QRCode(View view) {
        Intent intent = new Intent(this, GenqrActivity.class);
        startActivity(intent);
    }

}